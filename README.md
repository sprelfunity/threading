# Sprelf.Threading

Provides a Singleton MonoBehaviour "ThreadingManager", which makes it simple to perform actions or functions on an asynchronous thread, as well as to make it simple to queue up actions to perform back on the main thread during one of the Unity update phases:  Update(), LateUpdate(), or FixedUpdate().

## Dependencies

 - **SprelfSingleton**:  com.sprelf.singleton

## Remarks

If any of the update steps is not used, it is recommended to add preprocessor directives that will suppress ThreadingManager from checking the action queue every time.

See:

```
#define SUPPRESS_UPDATE_FUNCTION_CALLBACK
#define SUPPRESS_LATEUPDATE_FUNCTION_CALLBACK
#define SUPPRESS_FIXEDUPDATE_FUNCTION_CALLBACK
```
