using System;
using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using Sprelf.Threading;
using UnityEditor.SceneManagement;
using UnityEditor.UI;
using UnityEngine;
using UnityEngine.TestTools;

public class RuntimeTests_Threading
{
    [UnityTest]
    public IEnumerator Test_Coroutines()
    {
        bool handled = false;
        ThreadingManager.I.RunCoroutine(SimpleCoroutine(() => { handled = true; }));
        yield return new WaitForSecondsRealtime(0.02f);

        Assert.IsTrue(handled);

        handled = false;
        bool handled2 = false;
        ThreadingManager.I.RunCoroutine(SimpleCoroutine(() => { handled = true; }), () => { handled2 = true; }, (_) => { });
        yield return new WaitForSecondsRealtime(0.02f);

        Assert.IsTrue(handled);
        Assert.IsTrue(handled2);

        Exception e = new Exception();
        Exception thrown = null;
        handled = false;
        handled2 = false;
        ThreadingManager.I.RunCoroutine(ThrowingCoroutine(() => { handled = true; }, e),
                                        () => { handled2 = true; },
                                        (c) => { thrown = c; });
        yield return new WaitUntil(() => handled);

        Assert.IsTrue(handled);
        Assert.IsFalse(handled2);
        Assert.AreSame(e, thrown);
    }

    [UnityTest]
    public IEnumerator Test_YieldInstructions()
    {
        bool handled = false;
        ThreadingManager.I.RunCoroutine(new WaitForSecondsRealtime(0.01f), () => { handled = true; }, (_) => { });
        yield return new WaitForSecondsRealtime(0.015f);

        Assert.IsTrue(handled);
    }

    [UnityTest]
    public IEnumerator Test_CoroutineQueue()
    {
        bool handled = false;
        bool handled2 = false;
        ThreadingManager.I.ExecuteInUpdate(() => { handled = true; }, () => { handled2 = true; },
                                           updateType: ThreadingManager.UpdateType.COROUTINE);
        yield return new WaitForSecondsRealtime(0.02f);

        Assert.IsTrue(handled);
        Assert.IsTrue(handled2);

        handled = false;
        handled2 = false;
        Exception e = new Exception();
        Exception thrown = null;
        ThreadingManager.I.ExecuteInUpdate(() =>
                                           {
                                               handled = true;
                                               throw e;
                                           },
                                           () => { handled2 = true; },
                                           (c) => { thrown = c; },
                                           ThreadingManager.UpdateType.COROUTINE);
        yield return new WaitForSecondsRealtime(0.02f);

        Assert.IsTrue(handled);
        Assert.IsFalse(handled2);
        Assert.AreSame(e, thrown);
        
        
        handled = false;
        handled2 = false;
        thrown = null;
        ThreadingManager.I.ExecuteInUpdate(() =>
                                           {
                                               handled = true;
                                               throw e;
                                           },
                                           () => { handled2 = true; },
                                           (c) => { thrown = c; },
                                           ThreadingManager.UpdateType.COROUTINE_PACED);
        yield return new WaitForSecondsRealtime(0.02f);

        Assert.IsTrue(handled);
        Assert.IsFalse(handled2);
        Assert.AreSame(e, thrown);
    }

    //
    // HELPERS
    // ======================

    private IEnumerator SimpleCoroutine(Action action)
    {
        yield return null;

        action();

        yield return null;
    }

    private IEnumerator NullCoroutine()
    {
        yield break;
    }

    private IEnumerator ThrowingCoroutine(Exception e)
    {
        yield return null;

        throw e;

        yield return null;
    }

    private IEnumerator ThrowingCoroutine(Action action, Exception e)
    {
        yield return null;

        action();

        throw e;

        yield return null;
    }
}