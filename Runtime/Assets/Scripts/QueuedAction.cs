using System;

namespace Sprelf.Threading
{
    internal interface IQueuedAction
    {
        void Invoke();
        Action<Exception> OnError { get; set; }
    }

    internal struct QueuedAction : IQueuedAction
    {
        public Action Action { get; set; }
        public Action OnSuccess { get; set; }
        public Action<Exception> OnError { get; set; }

        public void Invoke()
        {
            try
            {
                Action();
                OnSuccess?.Invoke();
            }
            catch (Exception e)
            {
                if (OnError != null)
                    OnError(e);
                else
                    throw;
            }
        }
    }

    internal struct QueuedAction<T> : IQueuedAction
    {
        public Func<T> Action { get; set; }
        public Action<T> OnSuccess { get; set; }
        public Action<Exception> OnError { get; set; }

        public void Invoke()
        {
            try
            {
                T result = Action();
                OnSuccess?.Invoke(result);
            }
            catch (Exception e)
            {
                if (OnError != null)
                    OnError(e);
                else
                    throw;
            }
        }
    }
}