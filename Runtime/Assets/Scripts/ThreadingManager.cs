// #define SUPPRESS_UPDATE_FUNCTION_CALLBACK
// #define SUPPRESS_LATEUPDATE_FUNCTION_CALLBACK
// #define SUPPRESS_FIXEDUPDATE_FUNCTION_CALLBACK

using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading;
using Sprelf.Singleton;
using UnityEngine;

namespace Sprelf.Threading
{
    /// <summary>
    /// Handles running actions on functions on asynchronous threads, as well as providing methods for allowing those threads to
    /// return back to the main thread during one of the Unity update steps.
    ///
    /// <p>
    /// If any of the update steps (Update(), LateUpdate(), FixedUpdate()) are not used, it is recommended to add preprocessor directives
    /// to suppress their usage in order to marginally improve performance:
    /// </p>
    ///
    /// <code>
    /// #define SUPPRESS_UPDATE_FUNCTION_CALLBACK
    /// #define SUPPRESS_LATEUPDATE_FUNCTION_CALLBACK
    /// #define SUPPRESS_FIXEDUPDATE_FUNCTION_CALLBACK
    /// </code>
    /// </summary>
    public class ThreadingManager : Singleton<ThreadingManager>
    {
        /// <summary>
        /// Represents Unity update phase to execute an action in:  Update(), LateUpdate(), or FixedUpdate(), or in a
        /// coroutine loop.
        /// </summary>
        public enum UpdateType
        {
            UPDATE,
            LATE_UPDATE,
            FIXED_UPDATE,
            COROUTINE,
            COROUTINE_PACED
        }

        private readonly Dictionary<UpdateType, ConcurrentQueue<IQueuedAction>> _actionQueue =
            new Dictionary<UpdateType, ConcurrentQueue<IQueuedAction>>();

        protected override bool _destroyOnLoad => false;

        /// <summary>
        /// Initialize action queues
        /// </summary>
        protected override void OnAwake()
        {
            foreach (UpdateType ut in Enum.GetValues(typeof(UpdateType)))
                _actionQueue[ut] = new ConcurrentQueue<IQueuedAction>();
        }
        
        // COROUTINES
        // ==============================

        public void RunCoroutine(IEnumerator coroutine)
        {
            StartCoroutine(coroutine);
        }

        public void RunCoroutine(IEnumerator coroutine, Action onSuccess, Action<Exception> onError)
        {
            StartCoroutine(WrapCoroutine(coroutine, onSuccess, onError));
        }

        public void Wait(float seconds)
        {
            StartCoroutine(_Wait(seconds));
        }

        public void Wait(float seconds, Action onSuccess)
        {
            StartCoroutine(_Wait(seconds, onSuccess));
        }

        // public void RunCoroutine(Func<YieldInstruction> yieldInstruction)
        // {
        //     StartCoroutine(WrapYieldInstruction(yieldInstruction));
        // }
        //
        // public void RunCoroutine(Func<YieldInstruction> yieldInstruction, Action onSuccess, Action<Exception> onError)
        // {
        //     StartCoroutine(WrapCoroutine(WrapYieldInstruction(yieldInstruction), onSuccess, onError));
        // }

        // MAIN THREAD
        // ==============================

        /// <summary>
        /// Executes the given action during the optionally-specified update step on the main thread.  This may be safely invoked from any thread.
        /// Note that the action will not be performed immediately, but only the next time the appropriate update step is reached.
        /// </summary>
        /// <param name="action">The action to perform.</param>
        /// <param name="onSuccess">Optional.  A callback to perform upon success.</param>
        /// <param name="onError">Optional.  A callback to perform upon failure.</param>
        /// <param name="updateType">Optional.  The main thread update step to execute on.  Defaults to <see cref="UpdateType.COROUTINE"/>.</param>
        public void ExecuteInUpdate(Action action, Action onSuccess = null, Action<Exception> onError = null,
                                    UpdateType updateType = UpdateType.COROUTINE)
        {
            if (action == null)
                throw new ArgumentNullException(nameof(action));

            IQueuedAction queuedAction = new QueuedAction() {Action = action, OnSuccess = onSuccess, OnError = onError};

            ExecuteInUpdate(queuedAction, updateType);
        }

        /// <summary>
        /// Executes the given action during the optionally-specified update step on the main thread.  This may be safely invoked from any thread.
        /// Note that the action will not be performed immediately, but only the next time the appropriate update step is reached.
        /// </summary>
        /// <param name="action">The action to perform.</param>
        /// <param name="onSuccess">Optional.  A callback to perform upon success.</param>
        /// <param name="onError">Optional.  A callback to perform upon failure.</param>
        /// <param name="updateType">Optional.  The main thread update step to execute on.  Defaults to <see cref="UpdateType.COROUTINE"/>.</param>
        public void ExecuteInUpdate<T>(Func<T> action, Action<T> onSuccess = null, Action<Exception> onError = null,
                                       UpdateType updateType = UpdateType.COROUTINE)
        {
            if (action == null)
                throw new ArgumentNullException(nameof(action));

            IQueuedAction queuedAction = new QueuedAction<T>() {Action = action, OnSuccess = onSuccess, OnError = onError};

            ExecuteInUpdate(queuedAction, updateType);
        }

        private void ExecuteInUpdate(IQueuedAction queuedAction, UpdateType updateType)
        {
#if SUPPRESS_UPDATE_FUNCTION_CALLBACK
            if (updateType == UpdateType.UPDATE)
                throw new ThreadManagerException("Update step is suppressed.  Cannot queue actions for this step.")
#endif
#if SUPPRESS_LATEUPDATE_FUNCTION_CALLBACK
            if (updateType == UpdateType.LATE_UPDATE)
                throw new ThreadManagerException("LateUpdate step is suppressed.  Cannot queue actions for this step.")
#endif
#if SUPPRESS_FIXEDUPDATE_FUNCTION_CALLBACK
            if (updateType == UpdateType.FIXED_UPDATE)
                throw new ThreadManagerException("FixedUpdate step is suppressed.  Cannot queue actions for this step.")
#endif

            if (updateType == UpdateType.COROUTINE && _actionQueue[UpdateType.COROUTINE].IsEmpty)
            {
                // Some redundancy here to guard against potential race conditions that prevent the coroutine from starting
                _actionQueue[updateType].Enqueue(queuedAction);
                StartCoroutine(new FasterCoroutineHandler(_actionQueue[UpdateType.COROUTINE]));
            }
            else if (updateType == UpdateType.COROUTINE_PACED && _actionQueue[UpdateType.COROUTINE_PACED].IsEmpty)
            {
                _actionQueue[updateType].Enqueue(queuedAction);
                StartCoroutine(new CoroutineHandler(_actionQueue[UpdateType.COROUTINE_PACED]));
            }
            else
                _actionQueue[updateType].Enqueue(queuedAction);
        }

        // ASYNC
        // ==============================

        /// <summary>
        /// Executes the given action on a new thread.  All provided callbacks will be invoked on the main thread during
        /// the optionally-specified update step.
        /// </summary>
        /// <param name="action">The action to perform asynchronously</param>
        /// <param name="onError">A callback to invoke if an error occurs.</param>
        /// <param name="callbackUpdatePhase">Optional.  The main thread update step to execute any callbacks on.</param>
        /// <returns>
        /// Returns true if the action was successfully started on a new thread, false otherwise.
        /// </returns>
        public bool ExecuteAsync(Action action, Action<Exception> onError, UpdateType callbackUpdatePhase = UpdateType.COROUTINE)
        {
            return ExecuteAsync(action, () => { }, onError, callbackUpdatePhase);
        }

        /// <summary>
        /// Executes the given action on a new thread.  All provided callbacks will be invoked on the main thread during
        /// the optionally-specified update step.
        /// </summary>
        /// <param name="action">The action to perform asynchronously</param>
        /// <param name="onSuccess">A callback to invoke if the action completes successfully.</param>
        /// <param name="onError">A callback to invoke if an error occurs.</param>
        /// <param name="callbackUpdatePhase">Optional.  The main thread update step to execute any callbacks on.</param>
        /// <returns>
        /// Returns true if the action was successfully started on a new thread, false otherwise.
        /// </returns>
        public bool ExecuteAsync(Action action, Action onSuccess, Action<Exception> onError, UpdateType callbackUpdatePhase = UpdateType.COROUTINE)
        {
            return ExecuteAsync(new QueuedAction()
                                {
                                    Action = action,
                                    OnSuccess = () => ExecuteInUpdate(onSuccess, updateType: callbackUpdatePhase),
                                    OnError = (e) => ExecuteInUpdate(() => onError(e), updateType: callbackUpdatePhase)
                                });
        }

        /// <summary>
        /// Executes the given action on a new thread.  All provided callbacks will be invoked on the main thread during
        /// the optionally-specified update step.
        /// </summary>
        /// <param name="action">The action to perform asynchronously, returning a particular result.</param>
        /// <param name="onSuccess">A callback to invoke if the action completes successfully.  Receives the result from the given action as an argument.</param>
        /// <param name="onError">A callback to invoke if an error occurs.</param>
        /// <param name="callbackUpdatePhase">Optional.  The main thread update step to execute any callbacks on.</param>
        /// <returns>
        /// Returns true if the action was successfully started on a new thread, false otherwise.
        /// </returns>
        public bool ExecuteAsync<T>(Func<T> action, Action<T> onSuccess, Action<Exception> onError, UpdateType callbackUpdatePhase = UpdateType.COROUTINE)
        {
            return ExecuteAsync(new QueuedAction<T>()
                                {
                                    Action = action,
                                    OnSuccess = (x) => ExecuteInUpdate(() => onSuccess(x), updateType: callbackUpdatePhase),
                                    OnError = (e) => ExecuteInUpdate(() => onError(e), updateType: callbackUpdatePhase)
                                });
        }

        private static bool ExecuteAsync(IQueuedAction queuedAction)
        {
            try
            {
                ThreadPool.QueueUserWorkItem((_) => queuedAction.Invoke());
                return true;
            }
            catch (Exception e)
            {
                queuedAction.OnError?.Invoke(e);
                return false;
            }
        }

        // STATIC WRAPPERS
        // ==============================

        protected static IEnumerator _Wait(float seconds)
        {
            yield return new WaitForSeconds(seconds);
        }
        
        protected static IEnumerator _Wait(float seconds, Action onSuccess)
        {
            yield return new WaitForSeconds(seconds);

            onSuccess();
        }
        
        public static IEnumerator WrapCoroutine(IEnumerator coroutine, Action onSuccess, Action<Exception> onError)
        {
            yield return null;
            while (true)
            {
                try
                {
                    bool hasNext = coroutine.MoveNext();
                    if (!hasNext)
                    {
                        onSuccess();
                        break;
                    }
                }
                catch (Exception e)
                {
                    onError(e);
                    break;
                }

                yield return null;
            }
        }

        // UPDATE STEPS
        // ==============================

#if !SUPPRESS_UPDATE_FUNCTION_CALLBACK
        private void Update()
        {
            _DumpQueue(UpdateType.UPDATE);
        }
#endif

#if !SUPPRESS_LATEUPDATE_FUNCTION_CALLBACK
        private void LateUpdate()
        {
            _DumpQueue(UpdateType.LATE_UPDATE);
        }
#endif

#if !SUPPRESS_FIXEDUPDATE_FUNCTION_CALLBACK
        private void FixedUpdate()
        {
            _DumpQueue(UpdateType.FIXED_UPDATE);
        }
#endif

        // PRIVATE METHODS
        // ==============================

        /// <summary>
        /// Performs all queued actions for the given update type, clearing the queue for that update type.
        /// </summary>
        /// <param name="updateType">The update type to execute the queue for.</param>
        private void _DumpQueue(UpdateType updateType)
        {
            Queue<IQueuedAction> actions = new Queue<IQueuedAction>();
            ConcurrentQueue<IQueuedAction> queue = _actionQueue[updateType];
            while (queue.TryDequeue(out var action))
                actions.Enqueue(action);

            foreach (IQueuedAction action in actions)
                action.Invoke();
        }

        
    }
}