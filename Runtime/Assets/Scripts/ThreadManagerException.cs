using System;

namespace Sprelf.Threading
{
    public class ThreadManagerException : Exception
    {
        public ThreadManagerException() : base()
        {
        }

        public ThreadManagerException(string message) : base(message)
        {
        }
    }
}