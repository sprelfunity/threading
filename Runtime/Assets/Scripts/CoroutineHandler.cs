using System.Collections;
using System.Collections.Concurrent;
using System.Diagnostics;
using UnityEngine;

namespace Sprelf.Threading
{
    internal class CoroutineHandler : CustomYieldInstruction
    {
        public override bool keepWaiting
        {
            get
            {
                if (_queue.IsEmpty)
                {
                    IsDone = true;
                    return false;
                }
                
                _HandleQueue();
                
                return true;
            }
        }

        public bool IsDone { get; private set; } = false;

        protected readonly ConcurrentQueue<IQueuedAction> _queue;

        public CoroutineHandler(ConcurrentQueue<IQueuedAction> queue)
        {
            _queue = queue;
        }

        protected virtual void _HandleQueue()
        {
            if (_queue.TryDequeue(out IQueuedAction action))
                action.Invoke();
        }
    }

    internal class FasterCoroutineHandler : CoroutineHandler
    {
        public static float TimeoutWindow { get; set; } = 0.002f;

        public FasterCoroutineHandler(ConcurrentQueue<IQueuedAction> queue) : base(queue)
        {
        }

        protected override void _HandleQueue()
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            
            while (sw.Elapsed.TotalSeconds <= TimeoutWindow && _queue.TryDequeue(out IQueuedAction action))
                action.Invoke();

            sw.Stop();
        }
    }
}